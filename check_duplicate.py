
from typing import List
import os
import hashlib


def finde_duplikate(dateipfad1: str, dateipfad2: str) -> List:
    """
    Finde alle Dateien die sowohl in Dateipfad 1 als auch in Dateipfad 2 vorkommen
    und pruefe auf gleiche Dateinamen und gleichen Inhalt

    .
└── home
    └── user
        └── ayoub
            ├── test1
            │   ├── datei1.txt   MD5: 4711  # gleich zu test2
            │   ├── datei2.txt   MD5: 4712  # existiert nicht auf test2
            │   ├── datei3.txt   MD5: 7777  # gleicher Dateiname, anderer Inhalt
            │   └── geheim1.txt  MD5: 9999  # gleicher Inhalt anderer Dateiname
            └── test2
                ├── datei1.txt   MD5: 4711
                ├── datei3.txt   MD5: 8888
                ├── datei4.txt   MD5: 4714
                └── geheim2.txt  MD5: 9999

    Ergebnis:
    - Erklaerung siehe ungen
    - pfad ist immer /home/user/ayoub/test1 bzw. /home/user/ayoub/test2

    [[pfad, datei1.txt pfad, datei2.txt, True, True],
     [pfad, datei3.txt pfad, datei3.txt, True, False],
     [pfad, geheim1.txt pfad, geheim2.txt, False, True],
    ]
   
    Zur Aufgabe
    a) Loesung mit kleinem Beispiel
    b) Formatierung mit Pylint (PEP8)
    c) Schreiben von mehreren Tests
       c1) Verzeichnisse nicht gefunden / kein Leserecht
       c2) Dateien kein Leserecht
       c3) verschiedene Varianten von Dateien wie oben dargestellt


    :param dateipfad1: Verzeichnis in dem gesucht wird
    :param dateipfad2: Verzeichnis mit dem verglichen wird
    :return: Liste von Duplikaten in der Form:
             [ [dateipfad1: str, dateiname: str, dateipfad2: str, dateiname: str, name_gleich: Boolean, md5hash_gleich Boolean],
               [dateipfad1: str, dateiname: str, dateipfad2: str, dateiname: str, name_gleich: Boolean, md5hash_gleich Boolean],
               ...
             ]
    """

    #prüf, ob die Folder existiert oder nicht
    if os.path.exists(dateipfad1) and os.access(dateipfad1 , os.R_OK):
        files1 = [f for f in os.listdir(dateipfad1) if os.path.isfile(os.path.join(dateipfad1, f))]
    else:
        print("die erste Datei ist nicht existiert oder kein Zugriffrecht.")
        return
    if os.path.exists(dateipfad2) and os.access(dateipfad2 , os.R_OK):
        files2 = [f for f in os.listdir(dateipfad2) if os.path.isfile(os.path.join(dateipfad2, f))]
    else:
        print("die zweite Datei ist nicht existiert oder kein Zugriffrecht.")
        return
    if len(files1) == 0:
        print("die erste Datei ist leer.")
        return
    if len(files2) == 0:
        print("die zweite Datei ist leer.")
        return



    # list alle Dateien mit entsprechende chifrierte werten [ [filename , md5],[filename2,md5],...]

    datei1_hash_md5 = []
    datei2_hash_md5 = []

    # list alle deplikante Dateien [ [ dir1 ,file1, dir2 , fil2, boolean name ,boolean md5],[...],...]
    duplikateList = []

    #Öffnung und chiffierung der Dateien in erste Folder
    for filename in files1:
        try:
           with open(dateipfad1 + "\\" + filename, 'rb') as inputfile:
              data = inputfile.read()
              datei1_hash_md5.append([filename, hashlib.md5(data).hexdigest()])
        except OSError:
          print( "Could not open/read file:",dateipfad1 + "\\" + filename)
          return


   #Öffnung und chiffierung der Dateien in erste Folder
    for filename in files2:
        try:
          with open(dateipfad2 + "\\" + filename, 'rb') as inputfile:
             data = inputfile.read()
             datei2_hash_md5.append([filename, hashlib.md5(data).hexdigest()])
        except OSError:
           print( "kann nicht der Folder öffnen", dateipfad2 + "\\" +filename)
           return

     # prüf, ob es duplikante gibt
    for md1 in datei1_hash_md5:

        for md2 in datei2_hash_md5:
           if md2[0].__eq__(md1[0]) or md2[1].__eq__(md1[1]):
                duplikateList.append([dateipfad1, md1[0], dateipfad2, md2[0], md2[0].__eq__(md1[0]), md2[1].__eq__(md1[1])])

    return duplikateList


if __name__ == "__main__":
    # duplikate = finde_duplikate("/usr/ayoub/test1", "/usr/ayoub/test2")
    duplikate = finde_duplikate("C:/Users/alkha/test1/", "C:/Users/alkha/test2/")
    print(duplikate)
